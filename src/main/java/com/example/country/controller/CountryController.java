package com.example.country.controller;

import com.example.country.entity.Country;
import com.example.country.payload.DTO;
import com.example.country.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    CountryRepository countryRepository;

    @GetMapping
    public List<Country> getAll() {
        return countryRepository.findAll();
    }

    @GetMapping("/{id}")
    public Country get(@PathVariable Integer id) {
        return countryRepository.findById(id).orElseThrow(NullPointerException::new);
    }

    @PostMapping
    public String add(@RequestBody DTO countryDTO) {
        Country country = new Country();
        country.setName(countryDTO.getName());
        countryRepository.save(country);
        return "Successfully";
    }

    @PutMapping("/{id}")
    public String edit(@RequestBody DTO countryDTO, @PathVariable Integer id) {
        Country country = countryRepository.findById(id).orElseThrow(NullPointerException::new);
        country.setName(countryDTO.getName());
        countryRepository.save(country);
        return "Editing";
    }
}
